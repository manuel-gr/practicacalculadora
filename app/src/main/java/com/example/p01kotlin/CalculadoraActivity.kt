package com.example.p01kotlin

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class CalculadoraActivity : AppCompatActivity() {
    private lateinit var txtUsuario: TextView
    private lateinit var txtNum1 : EditText
    private lateinit var txtNum2 : EditText
    private lateinit var txtResultado : TextView

    private lateinit var btnSuma : Button
    private lateinit var btnResta : Button
    private lateinit var btnMulti : Button
    private lateinit var btnDivision : Button

    private lateinit var btnSalir : Button
    private lateinit var btnLimpiar : Button
    private lateinit var operaciones: Calculadora

    var opcion : Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_calculadora)
        iniciarComponentes()
        eventoClic()

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
    }
    public fun iniciarComponentes(){
        txtUsuario = findViewById(R.id.txtUsuario)
        txtResultado = findViewById( R.id.txtResultado)
        txtNum1 = findViewById(R.id.txtNum1)
        txtNum2 = findViewById( R.id.txtNum2)

        btnSalir = findViewById(R.id.btnSalir)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnDivision = findViewById( R.id.btnDivision)
        btnMulti = findViewById(R.id.btnMulti)
        btnSuma = findViewById( R.id.btnSuma)
        btnResta = findViewById(R.id.btnResta)

        val bundle : Bundle? = intent.extras

        txtUsuario.text = bundle?.getString("usuario")
    }

    public fun Validar():Boolean{
        if (txtNum1.text.toString().contentEquals("")
            || txtNum2.text.toString().contentEquals(""))
            return false
        else return true
    }

    public fun operaciones() :  Float{
        var num1:Float=0f
        var num2:Float=0f
        var res:Float=0f

        if (Validar()){
            num1 =txtNum1.text.toString().toFloat()
            num2 =txtNum2.text.toString().toFloat()
            operaciones =Calculadora(num1,num2)

            when(opcion){
                1->{
                    res=operaciones.sumar()
                }
                2->{
                    res=operaciones.resta()
                }
                3->{
                    res=operaciones.multi()
                }
                4->{
                    res=operaciones.division()
                }

            }

        }else
        {
            Toast.makeText(this, "Error Faltaron datos", Toast.LENGTH_SHORT).show()
        }
        return res
    }

    public fun eventoClic(){
        btnSuma.setOnClickListener(View.OnClickListener {
            opcion = 1
            txtResultado.text=operaciones().toString()
        })
        btnResta.setOnClickListener(View.OnClickListener {
            opcion = 2
            txtResultado.text=operaciones().toString()
        })
        btnMulti.setOnClickListener(View.OnClickListener {
            opcion = 3
            txtResultado.text=operaciones().toString()
        })
        btnDivision.setOnClickListener(View.OnClickListener {
            if (this.txtNum2.text.toString().toFloat() == 0f) {
                txtResultado.text = "No es posible dividir sobre 0"
            } else {
                opcion = 4
                txtResultado.text = operaciones().toString()
            }
        })
        btnSalir.setOnClickListener(View.OnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Calculadora")
            builder.setMessage("¿quieres salir?")

            builder.setPositiveButton(android.R.string.yes) { dialog, which ->
                this.finish()
            }
            builder.setNegativeButton(android.R.string.no) { dialog, which ->

            }
            builder.show()
        })
        btnLimpiar.setOnClickListener(View.OnClickListener {
            txtNum1.text.clear()
            txtNum2.text.clear()
            txtResultado.text = "Resultado"
        })
    }
}